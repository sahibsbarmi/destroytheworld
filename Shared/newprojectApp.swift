//
//  newprojectApp.swift
//  Shared
//
//  Created by Sahib Singh Barmi on 2021-02-21.
//

import SwiftUI

@main
struct newprojectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
